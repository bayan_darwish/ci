<?php

class Home extends CI_Controller {

    function index() {
        $data['view'] = "home";
        $this->load->view('layouts/main', $data);
    }
    function login_validation() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
        $this->index();
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
            $username = $this->input->post('usename');
            $password = $this->input->post('password');
            $user_id = $this->user_model->can_login($username, $password);
            if ($user_id) {
                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_in' => true
                );
                $this->session->set_userdata($user_data);
                $this->session->set_flasdata('login_success','you are now logged in');
            }
        }else{
//            $this->index();
        }
    }
    

}

?>