<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ci blog</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-3"><?php $this->load->view('users/login'); ?></div>
                <div class="col-xs-9"><?php // $this->load->view($view); ?></div>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>/assets/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
    </body>
</html>