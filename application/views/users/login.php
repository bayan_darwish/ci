<h1>login form</h1>
<?php $attributes = array('id' => "login-form", 'class' => "form-horizantal"); ?>


<?php echo validation_errors(); ?>
<?php echo form_open('home/login_validation', $attributes); ?>

<div class="form-group">
    <?php $data = array('class' => "form-control", 'name' => "username", 'placeholder' => "user name" , 'autocomplete' => "off"); ?>
    <?php echo form_label('Username'); ?>
    <?php echo form_input($data); ?>
    <span class="danger"><?php echo form_error('username');?></span>
</div>
<div class="form-group">
    <?php $data = array('class' => "form-control", 'name' => "password", 'placeholder' => "password" ); ?>
    <?php echo form_label('Password'); ?>
    <?php echo form_password($data); ?> 
    <span class="danger"><?php echo form_error('password');?></span>
</div>
<div class="form-group">
    <?php $data = array('class' => "form-control", 'name' => "confirm_password", 'placeholder' => "confirm password" ); ?>
    <?php echo form_label('Confirm Password'); ?>
    <?php echo form_password($data); ?>   
</div>
<div class="form-group">
    <?php $data = array('class' => "btn btn-primary", 'name' => "submit" , 'value' => "login"); ?>
    <?php echo form_submit($data); ?>   
</div>

<?php echo form_close(); ?>

